---
title: "What I'm Thinking About"
date: 2022-04-13T22:54:46-07:00
draft: false
---

- The semester at TMCC is about to start, and I'm looking forward to dealing with students again. This semester, I have decided that COVID has passed securely enough that I will look for an in-person job.

- I have switched from Dungeons & Dragons Fifth Edition to Pathfinder Second Edition for my bi-weekly group, and I can't stop thinking about how much more mechanically interesting and mathematically better it is. These thoughts will probably spill into a blog post at some point.
