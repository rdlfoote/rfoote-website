---
title: "About"
date: 2022-04-11T22:21:49-07:00
draft: false 
weight: 0
---

My name is Richard Foote, and I like programming, teaching, and writing mathematics and statistics. This website is here for me to share what I think and write about.

My LinkedIn and GitLab pages can be found in the navigation menu. {{< new-tab-link text="The source code of this website can be found here." link="https://gitlab.com/rdlfoote/rfoote-website" >}}

This website is built using themeless Hugo, raw CSS, and no javascript (for now). It is responsive and resists FOUC by using  static fonts and inline svgs. It uses the Computer Modern Bright font for text and the Computer Modern Typewriter Text font for code. The icons are from [svgrepo.com](https://svgrepo.com). It grows with my knowledge of web development, so there might be some kinks here and there.
