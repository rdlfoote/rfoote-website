#!/bin/bash

# If the first argument isn't an existent Rmd file, quit.
INPUTFILE="$1"
if [[ ! "$INPUTFILE" = *.Rmd || ! -f "$INPUTFILE" ]]; then
	echo Invalid File
	exit 1;
fi

# Knit the input file into the appropriate content directory.
#
# First, get the name of the target content directory from the input file name.
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
INPUTFILENAME=$(echo "$INPUTFILE" | sed 's/\.Rmd$//')
TARGETDIR="$DIR/../../content/blog/$INPUTFILENAME"
TARGETFILE="$TARGETDIR/index.md"

# If the target directory doesn't exist, create it.
if [ ! -d "$TARGETDIR" ]; then
	mkdir "$TARGETDIR"
fi

# Knit the input file and put the output in TARGETDIR.
R -e "knitr::knit(input = \"$INPUTFILE\", output = \"$TARGETFILE\")"
mv "$DIR"/figure "$TARGETDIR"

# Replace the \('s and \)'s in TARGETFILE with \\( and \\) to appease the
# markdown gods. Same for \['s and \]'s
sed -i 's/\\(/\\\\(/g' "$TARGETFILE"
sed -i 's/\\)/\\\\)/g' "$TARGETFILE"
sed -i 's/\\\[/\\\\\[/g' "$TARGETFILE"
sed -i 's/\\\]/\\\\\]/g' "$TARGETFILE"
