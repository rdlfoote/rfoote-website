# Source code of richardfoote.com

This is the source code of my website. It is built using themeless Hugo, raw 
css, and no javascript (for now). It is responsive and resists FOUC by using 
static fonts and inline svgs. It uses the Computer Modern Bright font for text 
and the Computer Modern Typewriter Text font for code. The icons are from 
[svgrepo.com](https://svgrepo.com). It grows with my knowledge of web 
development, so there might be some kinks here and there.
